const io = require("socket.io-client")


var socket = io.connect("http://localhost:3000/", {
    reconnection: true
});

socket.on('connect', function(){
    console.log("Connected to socket");
})


socket.on('TEST', (payload) => {
    console.log(payload);
})

socket.emit('READ_TAG', 'Emit works.');