"use strict";
const Mfrc522 = require("./node_modules/mfrc522-rpi/index");
const SoftSPI = require("rpi-softspi");
const io = require("socket.io-client")

var socket = io.connect('http://172.20.10.2:3000');

socket.on('connect', function(){
    console.log("Connected to socket");
})

socket.on('TEST', (payload) => {
    console.log(payload)
})

function emitTagEvent(message) {
    socket.emit('TAG_READ', message)
}






const softSPI = new SoftSPI({
  clock: 23, // pin number of SCLK
  mosi: 19, // pin number of MOSI
  miso: 21, // pin number of MISO
  client: 24 // pin number of CS
});

// GPIO 24 can be used for buzzer bin (PIN 18), Reset pin is (PIN 22).
// I believe that channing pattern is better for configuring pins which are optional methods to use.
const mfrc522 = new Mfrc522(softSPI).setResetPin(22).setBuzzerPin(18);

setInterval(function() {
  //# reset card
  mfrc522.reset();

  console.info(socket.connected)

  //# Scan for cards
  let response = mfrc522.findCard();
  if (!response.status) {
    console.log("No Card");
    return;
  }

  //# Get the UID of the card
  response = mfrc522.getUid();
  if (!response.status) {
    console.log("UID Scan Error");
    return;
  }


  //# If we have the UID, continue
  const uid = response.data;
  let idString = ""
  uid.forEach(id => {
    idString += id.toString()
  });
  
emitTagEvent(idString);
console.log(idString);



  //# Stop
  mfrc522.stopCrypto();
}, 500);
